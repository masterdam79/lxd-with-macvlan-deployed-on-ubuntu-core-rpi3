#!/usr/bin/env bash

RPI_IP="192.168.1.99"
UbuntuOneSSO="masterdam79"

ssh ${UbuntuOneSSO}@${RPI_IP} 'sudo lxd init --auto'
ssh ${UbuntuOneSSO}@${RPI_IP} 'lxc profile device set default eth0 nictype macvlan'
ssh ${UbuntuOneSSO}@${RPI_IP} 'lxc profile device set default eth0 parent eth0'
