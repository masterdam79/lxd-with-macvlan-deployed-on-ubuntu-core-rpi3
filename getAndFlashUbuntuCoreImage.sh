#!/usr/bin/env bash

IMAGE="ubuntu-core-18-armhf+raspi3.img.xz"

wget http://cdimage.ubuntu.com/ubuntu-core/18/stable/current/${IMAGE} -O ${IMAGE}

MMCBLK=$(cat /proc/partitions | grep mmc | head -1 | awk '{print $4}')

umount /dev/${MMCBLK}p*

xzcat ${IMAGE} | sudo dd of=/dev/${MMCBLK} bs=32M
