# lxd-with-macvlan-deployed-on-ubuntu-core-rpi3

BASH scripts to install LXD with MACvLAN with Ubuntu Core 18.04 on a Raspberry Pi3

# Assumptions
* The getAndFlashUbuntuCoreImage.sh script assumes that your MicroSD card is somewhere in /dev/mmc*

# Beware
* The getAndFlashUbuntuCoreImage.sh script will overwrite ANYTHING previously on the /dev/mmc* blockdevice, ALL DATA AND PARTITIONS WILL BE GONE!!
